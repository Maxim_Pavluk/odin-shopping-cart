export type User = {
  user: string | null;
  accessToken: string | null;
  refreshToken: string | null;
};
